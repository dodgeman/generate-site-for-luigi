#!/bin/bash

#
#
# $$\      $$\                           $$\                                                                    $$\
# \__|     $$ |                          $$ |                                                                   $$ |
# $$\ $$$$$$$ |$$$$$$\$$\    $$\ $$$$$$\ $$ |$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$\ $$$$$$\$$$$\  $$$$$$\ $$$$$$$\$$$$$$\
# $$ $$  __$$ $$  __$$\$$\  $$  $$  __$$\$$ $$  __$$\$$  __$$\$$  __$$\$$  __$$\$$  _$$  _$$\$$  __$$\$$  __$$\_$$  _|
# $$ $$ /  $$ $$$$$$$$ \$$\$$  /$$$$$$$$ $$ $$ /  $$ $$ /  $$ $$ /  $$ $$$$$$$$ $$ / $$ / $$ $$$$$$$$ $$ |  $$ |$$ |
# $$ $$ |  $$ $$   ____|\$$$  / $$   ____$$ $$ |  $$ $$ |  $$ $$ |  $$ $$   ____$$ | $$ | $$ $$   ____$$ |  $$ |$$ |$$\
# $$ \$$$$$$$ \$$$$$$$\  \$  /  \$$$$$$$\$$ \$$$$$$  $$$$$$$  $$$$$$$  \$$$$$$$\$$ | $$ | $$ \$$$$$$$\$$ |  $$ |\$$$$  |
# \__|\_______|\_______|  \_/    \_______\__|\______/$$  ____/$$  ____/ \_______\__| \__| \__|\_______\__|  \__| \____/
#                                                    $$ |     $$ |
#                                                    $$ |     $$ |
#                                                    \__|     \__|

####################################################################################################
#### author: Hugo Tachoires & ideveloppement #######################################################
#### link: https://www.ideveloppement.fr/ ##########################################################
#### destination: Can be run everywhere. And for ideveloppement developers <3 ######################
#### purpose: Generation of project directories, automation of apache and rights configurations ####
####################################################################################################

# CC <=> Clear Color
CC='\033[0m'

# Y <=> Yellow
Y='\033[1;33m'

# R <=> Red
R='\033[0;31m'

# LGRE <=> Light Green
LGRE='\033[1;32m'

# LGRA <=> Light gray
LGRA='\033[0;37m'

error_and_exit() {
  echo -e "${R}$1$CC" >&2
  exit 1
}

# Checks if exactly 3 parameters have been given
if [ $# -ne 3 ]; then
  error_and_exit "3 parameters must be provided. $# given."
fi

# Who are you ?
home_name=$2
home_directory="/home/$home_name"

# Checks if you exist
if ! [ -d "$home_directory" ]; then
  error_and_exit "Could not find '$home_name' directory in home directory."
fi

project_name=$3

# Example: /home/james/super-project
project_directory="$home_directory/$project_name"

echo -e "Hello $Y${home_name^}$CC !"

go_to() {
  cd "$1" || error_and_exit "Cannot go to $1"
}

step_create() {
  if [ -d "$project_directory" ]; then
    error_and_exit "Project named '$project_name' already exists."
  fi

  mkdir "$project_directory"

  go_to "$project_directory"

  mkdir www logs

  cat >"$project_name.vhost.conf" <<EOF
<VirtualHost *:80>
        ServerAdmin barney@ideveloppement.fr
        ServerName $home_name.$project_name.luigi.id

        DocumentRoot $project_directory/www/web
        <Directory />
                Options FollowSymLinks
                AllowOverride None
        </Directory>
        <Directory $project_directory/www/>
                Options -Indexes +FollowSymLinks +MultiViews
                AllowOverride All
		Require all granted
        </Directory>

        ErrorLog $project_directory/logs/error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog $project_directory/logs/access.log combined
</VirtualHost>
EOF

  echo -e "${LGRE}Deployment '$project_directory' created.$CC"
  cat <<EOF
Run: 'cd $project_directory' to go in your project directory.

You can now upload your Symfony project in '$project_directory/www'.
After that, you can run apache step to add your site in apache:
 ./generate.sh apache $home_name $project_name
EOF
}

step_apache() {
  if ! [ -d "$project_directory" ]; then
    error_and_exit "Project named '$project_name' cannot be found."
  fi

  go_to "$project_directory"

  # Filename in sites-available
  apache_conf_path="/etc/apache2/sites-available/$home_name.$project_name.conf"

  # Filename in project directory
  local_conf_path="$project_directory/$project_name.vhost.conf"

  ln -s "$local_conf_path" "$apache_conf_path" &&
    a2ensite "$home_name.$project_name.conf" &&
    printf "Running apache2 reload..." &&
    service apache2 reload && echo -e "${LG}OK$CC" && return 0 || return 1
}

step_rights() {
  for directory in web var; do
    path=$project_directory/www/$directory
    if ! [ -d "$path" ]; then
      echo -e " ${LGRA}Skipping '$path' directory (not found).$CC"
      continue
    fi

    printf " Processing '%s' rights..." "$directory" &&
      chown -R 33:33 "$path" && # Set www-data to group and owner
      chmod -R 775 "$path" && # Update rwx rights
      echo -e "$LGRE'$path' directory updated.$CC"
  done
}

case $1 in
create)
  step_create
  ;;
apache)
  if step_apache; then
    echo "Running rights step..."
    step_rights
  fi
  ;;
rights)
  step_rights
  ;;
*)
  echo >&2 "Unknown step. Use 'create', 'apache' or 'rights'"
  ;;
esac
