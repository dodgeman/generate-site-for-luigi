# Création de déploiement sur Luigi

Ce script a comme but de ne plus lancer de commande à la main
pour créer un déploiement sur le serveur Luigi, **génial nan ?**

Pour ce faire, plusieurs steps sont mis à disposition dans le script `generate.sh`.

- `create` pour créer le dossier avec la conf apache avec le nom du projet,
- `apache` ajoute le lien symbolique, enable le site et reload apache,
- `rights` Change les droits des dossiers `web` et `var`.

## Step Create

Cette partie détail les étapes du step `create` :

- Création du dossier du projet,
- Création des dossiers `www`, `logs` et du fichier `*projet*.vhost.conf`.

## Step Apache

Cette partie détail les étapes du step `apache` :

- Création du lien symbolique de la conf du projet dans le dossier `sites-available` d'apache,
- Activation du site et reload d'apache,
- **Lancement du step Rights**.

## Step Rights

Cette partie détail les étapes du step `rights` :

- Changement du groupe et owner des dossiers `var` et `web` en **_www-data_** du dossier `wwww`.

## Exemples d'utilisation

Pour exécuter le script il faut exactement 3 arguments :

- Le step voulu (`create`, `apache` ou `rights`),
- Le nom du dossier home voulu (james, mouton, jean…),
- Le nom du projet qui sera utilisé pour créer le dossier et la config apache (idev, socle, babyfoot…). 

### Utilisation du step `create`

La commande :

```
./generate.sh create james idev
```

Génère dans `/home/james/idev` :

```
root@luigi:~# tree idev/
idev/
|-- idev.vhost.conf
|-- logs
`-- www

2 directories, 1 file
```

Avec la configuration vhost suivante :

```
<VirtualHost *:80>
        ServerAdmin barney@ideveloppement.fr
        ServerName james.idev.luigi.id

        DocumentRoot /home/james/idev/www/web
        <Directory />
                Options FollowSymLinks
                AllowOverride None
        </Directory>
        <Directory /home/james/idev/www/>
                Options -Indexes +FollowSymLinks +MultiViews
                AllowOverride All
                Require all granted
        </Directory>

        ErrorLog /home/james/idev/logs/error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog /home/james/idev/logs/access.log combined
</VirtualHost>
```

Remarque : Apache n'a toujours aucune information sur le projet actuellement généré.

### Utilisation du step `apache`

Je vous conseille de lancer cette commande après le déploiement des sources Symfony dans le dossier `www` pour passer directement `step rights`.

La commande :

```
./generate.sh apache james idev
```

Génère le lien symbolique de la vhost du projet généré lors du `step create`.
De plus, elle enable le site et reload apache comme une grande :

```
root@luigi:~# ./generate.sh apache james idev 
Hello James !
Enabling site james.idev.
To activate the new configuration, you need to run:
  service apache2 reload
Running apache2 reload...OK
Running rights step...
 Processing 'web' rights...'/home/james/idev/www/web' directory updated.
 Processing 'var' rights...'/home/james/idev/www/var' directory updated.
```

Remarque : Comme vous avez pu le constater, si le déploiement d'apache se passe bien, le `step rights` est automatiquement lancé :)

### Utilisation du step `rights`

Le `step rights` permet de changer les droits sur les dossiers `web` et `var` s'ils sont présents dans le dossier `www` du projet donné en entrée.
Les droits sont donnés à 33, le fameux **www-data**.

```
root@luigi:~# ./generate.sh rights james idev
Hello James !
 Processing 'web' rights...'/home/james/idev/www/web' directory updated.
 Processing 'var' rights...'/home/james/idev/www/var' directory updated.
```

Remarque : Le changement de droit est indépendant, `var` ne dépend pas de `web` et inversement, évidant non ?
